# OpenML dataset: Heart-Disease-Dataset-(Comprehensive)

https://www.openml.org/d/43672

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Heart Disease Dataset (Most comprehensive)
Content
Heart disease is also known as Cardiovascular diseases (CVDs) are the number 1 cause of death globally, taking an estimated 17.9 million lives each year which is about 32 of all deaths globally. CVDs are a group of disorders of the heart and blood vessels and include coronary heart disease, cerebrovascular disease, rheumatic heart disease, and other conditions. Four out of 5CVD deaths are due to heart attacks and strokes, and one-third of these deaths occur prematurely in people under 70 years of age.
We have curated this dataset by combining different datasets already available independently but not combined before. W have combined them over 11 common features which makes it the largest heart disease dataset available for research purposes. The five datasets used for its curation are:
Database:                 of instances:

Cleveland:                                          303
Hungarian:                                         294
Switzerland:                                       123
Long Beach VA:                                 200
Stalog (Heart) Data Set:                    270

Total                                            1190
Acknowledgements
The dataset is taken from three other research datasets used in different research papers. The Nature article listing heart disease database and names of popular datasets used in various heart disease research is shared below.
https://www.nature.com/articles/s41597-019-0206-3
Inspiration
Can you find interesting insight from the largest heart disease dataset available so far and build predictive model which can assist medical practitioners in detecting early-stage heart disease ?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43672) of an [OpenML dataset](https://www.openml.org/d/43672). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43672/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43672/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43672/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

